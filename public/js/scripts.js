ActivePage($('.navbar li a'), 'active', true);
ActivePage($('.sidebar li a'), 'active', true);
AjaxModal();
var request;

function ActivePage(links, cls, parent) {
    $(links).each(function (i, link) {
        link = $(link);
        var url = link.attr('href');
        var path = window.location.href.toString();

        var after = getUrlParameter("after");
        var path2 = domain + "/" + after;

        var parts = path.split('/');
        var str = path.replace('/' + parts.pop(), '');

        url = baseUrl(url);

        if (baseUrl(path) === url || url === str || path2 === url) {
            if (!parent) {
                link.addClass(cls)
            } else {
                link.parent().addClass(cls)
            }
        } else {
            if (!parent) {
                link.removeClass(cls)
            } else {
                link.parent().removeClass(cls)
            }
        }

    });
}

function baseUrl(url) {
    return url.split('?')[0];
}

$('#_new').click(function () {
    $('#table').toggle();
});

function getUrlParameter(sParam) {
    // https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function AjaxModal() {

    $(".ajaxModal").click(function (event) {

        event.preventDefault();

        url = $(this).attr('href');

        var modal = $('#modal');
        var location = $('#modal-yield');

        if (request) {
            request.abort();
        }

        request = $.ajax({
            url: url,
            type: "get",
        });

        var loader = '<i class="">Loading</i>';
        location.html(loader + ' Please wait...');
        modal.modal({'backdrop': 'static'});

        request.done(function (response, status, xhr) {
            location.html(response);
        });

        request.fail(function (xhr, status) {
            var error = xhr.responseText;

            if (xhr.status == 401) {
                window.location = domain + "/login";
            } else {
                location.html(error);
            }
        });

        request.complete(function (xhr) {

        });

        request.always(function () {
        });
    });
}

function logOut() {
    if (confirm('Are you sure you want to logout?')) {
        document.getElementById('logout-form').submit();
    }
}
