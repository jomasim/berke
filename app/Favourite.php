<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $fillable = [
        'member_id', 'quote',
    ];

    protected $appends = ['details', 'benefits'];

    protected $hidden = ['quote', 'member_id'];

    public function getQuoteAttribute($value)
    {
        return json_decode($value);
    }

    public function setQuoteAttribute($value)
    {
        $this->attributes['quote'] = json_encode($value);
    }

    public function getAmount($insurer)
    {
        if ($this->quote->cover == 'comprehensive') {
            $charge = $insurer->getBasicRate($this->quote->cover, $this->quote->category) / 100
                * $this->quote->estimated_value;
        } else {
            $charge = $insurer->getBasicRate($this->quote->cover, $this->quote->category);
        }

        return (int)$charge;
    }

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function getDetailsAttribute()
    {
        $quote = $this->quote;
        $quote->insurer = Insurer::find($this->quote->insurer_id);
        $quote->amount = number_format($this->getAmount($quote->insurer), 2);
        return collect($quote)
            ->except('insurer_id');
    }

    public function getBenefitsAttribute()
    {
        return Insurer::find($this->quote->insurer_id)->benefits;
    }
}
