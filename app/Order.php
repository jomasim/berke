<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    protected $fillable = ['order', 'member_id', 'reference'];
    protected $casts = ['created_at' => 'datetime:Y-m-d'];
    protected $appends = ["insurer", "payment"];

    public function setOrderAttribute($value)
    {
        $this->attributes['order'] = json_encode($value);
    }

    public function getOrderAttribute($value)
    {
        return json_decode($value);
    }

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function getPaymentAttribute()
    {
        return Payment::where("reference", $this->reference)->first();
    }

    public function getInsurerAttribute()
    {
        return Insurer::find($this->order->insurer_id);
    }

    public function getCreatedAtAttribute($value)
    {
        return (new Carbon($value))->format('d-m-Y');
    }
}
