<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable=[
        'make','model','yom','estimated_value','cover','yor','category'
    ];
}
