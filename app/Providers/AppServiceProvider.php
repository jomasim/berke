<?php

namespace App\Providers;

use App\Payment;
use App\Events\PaymentCompleted;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Payment::created(function ($payment) {
            event(new PaymentCompleted($payment));
        });
        require_once app_path("Helpers/helpers.php");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Schema::defaultStringLength(191);
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
