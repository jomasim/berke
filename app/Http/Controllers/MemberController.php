<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Member;

class MemberController extends Controller
{
    public function CreateMember(Request $request)
    {
        $member = new Member();
        $member->fill($request->all());
        $member->password = bcrypt($request->password);
        $member->save();
        return response()->json($member);
    }

    public function whoami()
    {
        return auth()->user();
    }
}
