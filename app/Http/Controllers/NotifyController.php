<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Mail\OrderConfirmed;

class NotifyController extends Controller
{
    public function sendReminder()
    {
        $order = Order::find(20);
        $response = \Mail::to("simjoms@gmail.com")
            ->send(new OrderConfirmed($order));
        return $response;
    }
}
