<?php

namespace App\Http\Controllers;

use App\Insurer;
use Illuminate\Http\Request;
use Validator;

class QuotesController extends Controller
{
    public function quote(Request $request)
    {
        $quotes = [];
        foreach (Insurer::all() as $insurer) {

            if ($request->cover == 'comprehensive') {
                $charge = $request->estimated_value * ($insurer->getBasicRate($request->cover, $request->category) / 100);
            } else {
                $charge = $insurer->getBasicRate($request->cover, $request->category);
            }
            if ($charge != null) {
                $quotes[] = ["id" => $insurer->id,
                    "insurer" => $insurer->insurer_name,
                    "cover" => $request->cover,
                    "category" => $request->category,
                    "estimated_value" => $request->estimated_value,
                    "quote" => number_format($charge, 2),
                    "benefits" => $insurer->benefits,
                ];

            }
        }
        return response()->json($quotes);
   }
}


