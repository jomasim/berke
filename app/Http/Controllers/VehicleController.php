<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use Validator;

class VehicleController extends Controller
{
    public function addVehicle(Request $request)
    {
        $vehicle = new Vehicle();
        $vehicle->fill($request->all())->save();
       return response()->json(['status'=>'success']);
    }
}
