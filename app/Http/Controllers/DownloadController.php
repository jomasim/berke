<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DownloadController extends Controller
{
    public function getDownload()
    {
        $header["Content-Type"] = "application/vnd.android.package-archive";
        $file = public_path()."/download/berke.apk";
        return response()->download($file, "berke.apk", $header);

    }
}
