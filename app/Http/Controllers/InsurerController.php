<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Insurer;

class InsurerController extends Controller
{
    public function addInsurer(Request $request)
    {
        $insurer = new Insurer();
        $insurer->fill($request->all())->save();
    }
}
