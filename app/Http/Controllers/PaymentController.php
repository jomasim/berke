<?php

namespace App\Http\Controllers;

use App\Events\PaymentCompleted;
use App\Member;
use App\Mpesa;
use App\Order;
use App\Payment;
use Illuminate\Http\Request;
use App\Mail\OrderConfirmed;


class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['mpesaValidate', 'mpesaConfirm', 'webPay']]);
    }

    public function registerUrls()
    {
        $mpesa = new Mpesa\Mpesa();
        return $mpesa->registerCb2Url();
    }

    public function testPay()
    {
        $mpesa = new Mpesa\Mpesa();

        $ShortCode = '600000';
        $CommandID = "CustomerPayBillOnline";
        $amount = 4500;
        $Mssidn = 254708374149;
        $BillRefNumber = "602967";


        $response = $mpesa->c2b($ShortCode, $CommandID, $amount, $Mssidn, $BillRefNumber);
        $result = json_decode($response, true);
        return $result;

    }

    public function webPay(Request $request)
    {
        $mpesa = new Mpesa\Mpesa();

        $ShortCode = '965457';
        $CommandID = "CustomerPayBillOnline";
        $amount = $request->get('amount');
        $Mssidn = $request->get('phone');
        $BillRefNumber = rand(100000, 1000000);

        $out = $mpesa->c2b($ShortCode, $CommandID, $amount, $Mssidn, $BillRefNumber);
        $response = json_decode($out, true);
        $message = "transaction was successful";
        return view('mpesa', compact('message', 'response'));
    }

    public function mpesaValidate(Request $request)
    {
        $result = ["ResultCode" => "0", "Description" => "success"];
        return response()->json($result, 200);
    }

    public function mpesaConfirm(Request $request)
    {
        $result = ["ResultCode" => "0", "Description" => "success"];

        $payment = new Payment();

        $payment->payment_id = $request->json("TransID");
        $payment->amount = $request->json("TransAmount");
        $payment->customer_name = $request->json("FirstName") . " " .
            $request->json("MiddleName") . " " .
            $request->json("LastName");
        $payment->customer_contact = $request->json("MSISDN");
        $payment->reference = $request->json("BillRefNumber");
        $payment->payee = env("BUSINESS_NAME") . "-" . $request->json("BusinessShortCode");
        $payment->pay_method = "mpesa";

        $payment->save();
        return response()->json($result, 200);
    }

    public function confirmPayment(Request $request)
    {
        $content = Payment::where("customer_contact", $request->get("customer_contact"))
            ->where("status", 0)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($content) {
            $payload = ["payload" => $content, "user" => auth()->user()];
            return response()->json($payload, 200);
        } else {
            abort(400, "error");
        }
    }

    public function saveOrder(Request $request)
    {
        $order = new Order();
        $order->status = "paid";
        $order->order = collect($request->except('reference'));
        $order->reference = $request->get('reference');

        $order = Member::find(auth()->user()->id)
            ->orders()
            ->save($order);

        \Mail::to("simjoms@gmail.com")
            ->queue(new OrderConfirmed($order));

        return $order;

    }

    public function getSaveOrders()
    {
        $order = Member::findOrFail(auth()->user()->id)->orders()->with('member')->first();
        return response()->json($order, 200);
    }
}
