<?php

namespace App\Http\Controllers;

use App\Insurer;
use App\Member;
use Illuminate\Http\Request;
use App\Favourite;
use Validator;


class FavouriteController extends Controller
{

    function addFavourite(Request $request)
    {
        Member::find(auth()->user()->id)
            ->favourites()
            ->save(new Favourite(['quote' => collect($request->all())]));
        return response()->json(["status"=>'success']);

    }

    function getFavourite()
    {
        $favourite = Member::findOrFail(auth()->user()->id)->favourites()->with('member')->get();
        return response()->json($favourite);
    }
}
