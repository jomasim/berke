<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Insurer extends Model
{
    protected $fillable = [
        'insurer_name', 'insurer_id'
    ];

    public function settings()
    {
        return $this->hasMany('App\Setting');
    }

    public function benefits()
    {
        return $this->hasMany('App\Benefits');
    }

    public function getBasicRate($cover, $category)
    {
        $data = $this->settings()->whereCover($cover)->whereMotorCategory($category)->pluck('value')->first();
        return $data;
    }

    public function getFixedCharge()
    {
        $data = DB::table($this->insurer)->where('variable_name', 'fixed_charge')->get()->pluck('value')->first();
        return $data;
    }

    public function getPersonalCharge()
    {
        $data = DB::table($this->insurer)->where('setting_id', 'like', 'TVPP%')->get()->pluck('value')->first();
        return $data;
    }

}
