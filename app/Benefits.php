<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefits extends Model
{
    //
    protected $hidden = [
        'insurer_id'
    ];
}
