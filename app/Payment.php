<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $hidden = ['id', 'updated_at'];

    protected $casts = ['created_at' => 'datetime:Y-m-d'];

    public function getPayeeAttribute($value)
    {
        $exploded = explode("-", $value);
        return $exploded[0] . " Brokers";
    }

    public function getCreatedAtAttribute($value)
    {
        return (new Carbon($value))->format('d-m-Y');
    }
}

