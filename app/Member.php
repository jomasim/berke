<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Member extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'names', 'email', 'phone', 'username',
    ];
    protected $hidden = ['password', 'created_at', 'updated_at'];

    public function favourites()
    {
        return $this->hasMany('App\Favourite');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }

    public function getJWTIdentifier()
    {
        return $this->id;
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


}
