<?php

if (!function_exists('coverage')) {
    function coverage($type)
    {
        if (preg_match("#^C#i", $type) === 1) {
            $cover_type = "Comprehensive";
        } elseif (preg_match("#^T#i", $type) === 1) {
            $cover_type = "Third Party";
        }
        return $cover_type;
    }
}
if (!function_exists('is_psv')) {
    function is_psv($type)
    {
        if (preg_match("#^'TV#i", $type)) {
            return true;
        } else {
            return false;
        }
    }
}