<!DOCTYPE html>
<html>
<head>
    <title>Berke</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/styles.css')}}">

</head>
<body>
<div class="wrapper">
    @auth
        <nav class="sidebar">
            <ul>
                <li class="bg-primary" style="padding: 20px">
                    <b>{{auth()->user()->name}}</b>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>

                    <button class="btn btn-sm btn-danger" onclick="logOut()">Logout</button>
                </li>
                <li><a @if(request()->after =='activity')
                       style="color: inherit !important; font-weight: normal"
                       @endif href="{{url('member')}}"><img src="{{url('imgs/user.svg')}}">Profile</a></li>
                <li><a href="{{url('activity')}}"><img src="{{url('imgs/layers.svg')}}">Activities</a></li>
                <li><a href="{{url('services')}}"><img src="{{url('imgs/link.svg')}}">Ext-Services</a></li>
                <li><a href="{{url('reports')}}"><img src="{{url('imgs/folder.svg')}}">Business Intel</a></li>
                <li><a href="{{url('settings')}}"><img src="{{url('imgs/settings.svg')}}">Configuration</a></li>

                <li><a href="{{url('docs')}}">CPS API Docs <i>v1.0.0</i></a></li>
            </ul>
        </nav>
    @endauth
    <div class="content">
        @include('alerts')
        @yield('content')
    </div>
</div>
</body>

<script src="{{url('js/jquery.js')}}"></script>
<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="{{url('js/scripts.js')}}"></script>
@yield('js')
@include('include.echo')

</html>