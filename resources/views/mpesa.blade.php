@extends('report')
@section('data')
    @if(isset($message))

        <div>
            <h3 style="color: #c7254e">{{$message}}</h3>
        </div>
    @endif
    <div class="container text-center">
        <div><h2>Test Pay Bill:600000</h2></div>
        <div><h2>Test phone:254708374149///</h2></div>
        <div class="row">
            <div class="col-sm-4">

                <form action={{url('/mpesa-test')}} , method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input name="phone" , type="phone" class="form-control" placeholder="Phone Number" required>
                    </div>
                    <div class="form-group">
                        <input name="amount" , type="number" class="form-control" placeholder="amount" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="form-control" name="pay" , value="Make Payment">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection