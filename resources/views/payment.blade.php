@extends('report')
@section('data')
    @php
        $payments=\App\Payment::all()->sortByDesc('created_at');
    @endphp
    <table class="table table-striped" id="payments-table">
        <thead>
        <tr>
            <th>Transaction ID</th>
            <th>Customer Name</th>
            <th>Amount</th>
            <th>Customer Contact</th>
            <th>Reference</th>
            <th>Date</th>
        </tr>
        </thead>
        <tr>
        </tr>
        @if(isset($payments))
            @foreach($payments as $payment)
                <tr>
                    <td>{{ $payment->payment_id }}</td>
                    <td>{{ $payment->customer_name }}</td>
                    <td>{{ $payment->amount}}</td>
                    <td>{{ $payment->customer_contact}}</td>
                    <td>{{ $payment->reference}}</td>
                    <td>{{ (new \Carbon\Carbon($payment->created_at))->format('d-m-Y')}}</td>
                </tr>
            @endforeach
        @endif
    </table>
@endsection