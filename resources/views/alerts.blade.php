@if(count($errors) > 0)
    <div class="alert alert-danger">
        <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('ok'))
    <div class="main" style="margin-bottom: 0;">
        <div class="alert  alert-success">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session('ok')}}
        </div>
    </div>
@endif

@if(session()->has('error'))
    <div class="main" style="margin-bottom: 0;">
        <div class="alert alert-danger">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session('error')}}
        </div>
    </div>
@endif