<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Berke</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;

        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .h2 {
            width: 50%;
            margin: 0;
            padding: 0;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
            color: #101010;
            text-decoration: none;
            font-weight: bold;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }
    </style>
</head>
<body>
<div class="content">
    @php
        $member=\App\Member::find($order->member_id);
    @endphp
    <div class="top-right" style="text-align: left">
        <h5>PO BOX 14563-100,Nairobi</h5>
        <h5>Tel :0728109567</h5>
    </div>
    <div>
        <h4 style="text-align: left"> Berke Insurance Brokers Limited</h4>
        <h4 style="text-align: left">{{"Reference No: ". $order->reference}}</h4>
        <h4 style="text-align: left">{{"Date : ". $order->created_at}}</h4>
    </div>
    <hr>
    <div>
        <div style="height: 20%;background-color: #afd9ee">
            <h3>Customer</h3>
        </div>
        <table>
            <tr>
                <td>Name</td>
                <td>{{$member->username}}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{$member->email}}</td>
            </tr>
            <tr>
                <td>Contact</td>
                <td>{{$member->phone}}</td>
            </tr>
        </table>
    </div>

    <div>
        <div style="height: 20%;background-color: #afd9ee">
            <h3>Cover Particulars</h3>
        </div>
        @php
            $insurer=\App\Insurer::find($order->order->insurer_id);
        @endphp
        <table>
            <tr>
                <td>{{$insurer->insurer_name. " ". "Insurance"}}</td>
                <td>{{$order->order->cover}}</td>
                <td>{{"Ksh ".$order->order->value."/="}}</td>
            <tr>
                <td>{{"Status:"}}</td>
                <td>{{$order->status}}</td>
                <td>{{"Date :".$order->created_at}}</td>
            </tr>
            <tr>
                <td>{{"Commencing Date :".$order->created_at}}</td>
                <td>{{"Expiry Date :".(new Carbon\Carbon($order->created_at))->addMonth()->toDayDateTimeString()}}</td>
            </tr>
        </table>

    </div>

    <div>
        <div style="height: 20%;background-color: #afd9ee">
            <h3>Payment</h3>
        </div>
        @php
            $content = \App\Payment::where("reference", $order->reference)
           ->where("status", 0)
           ->orderBy('created_at', 'desc')
           ->first();
        @endphp
        <table>
            <tr>
                <td>{{"Customer Name"}}</td>
                <td>{{$content->customer_name}}</td>
            <tr>
                <td>{{"Amount Paid"}}</td>
                <td>{{"Ksh ".$content->amount."/="}}</td>
                <td>{{"To ".$content->payee}}</td>
            </tr>
            <tr>
                <td>{{"Paid Via ".$content->pay_method}}</td>
                <td>{{"Trans ID :".$content->payment_id}}</td>
                <td>{{"Date :".$content->created_at}}</td>
            </tr>
        </table>
        <hr>
        <small>Berke Insurance Brokers Limited</small>
        <hr>
    </div>
</div>
</body>
</html>
