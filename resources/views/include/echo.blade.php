<script src="{{url('js/pusher.min.js')}}"></script>
<script src="{{ url('js/echo.js') }}"></script>
<script src="{{ url('js/notify.js') }}"></script>

<script>
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: '{{ env('PUSHER_APP_KEY') }}',
        cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
        encrypted: true
    });


    Pusher.logToConsole = true;
    Echo.channel('payment')
        .listen('PaymentCompleted', function (e) {
            console.log(e);
            var table = document.getElementById('payments-table');
            var tbody = table.getElementsByTagName('tbody')[0];
            var row = tbody.insertRow(1);

            row.insertCell(0).innerHTML = e.payment_id;
            row.insertCell(1).innerHTML = e.customer_name;
            row.insertCell(2).innerHTML = e.amount;
            row.insertCell(3).innerHTML = e.customer_contact;
            row.insertCell(4).innerHTML = e.reference;
            row.insertCell(5).innerHTML = e.created_at;
            row.style.fontWeight = "bold";
        });
</script>
