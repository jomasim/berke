@extends('app')
@section('content')
    <header class="header">
        <h2>Berke Insurance Brokers Ltd</h2>
        <br>
    </header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li><a href="{{url('reports/mpesa-test')}}">Mpesa Test Page</a></li>
            </ul>
        </div>
    </nav>
    <div id="yield">
        @yield('data')
    </div>
@endsection