<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/create/insurer', 'InsurerController@addInsurer');
Route::post('/create/member', 'MemberController@CreateMember');
Route::get('/whoami', ['middleware' => 'auth:api', 'uses' => 'MemberController@whoami']);
Route::post('/auth', 'Auth\LoginController@login');
Route::post('/add/vehicle', 'VehicleController@addVehicle');
Route::get('/quote', ['middleware' => 'auth:api', 'uses' => 'QuotesController@quote']);
Route::post('/add/favourite', ['middleware' => 'auth:api', 'uses' => 'FavouriteController@addFavourite']);
Route::get('/favourites', ['middleware' => 'auth:api', 'uses' => 'FavouriteController@getFavourite']);
Route::get('/payments', 'PaymentController@pay');
Route::post('/test-pay', 'PaymentController@webPay');
Route::post('/register-urls', 'PaymentController@registerUrls');
Route::post('/order', 'PaymentController@saveOrder');
Route::post('/payments', 'PaymentController@mpesaResponse');
Route::post('/validate', 'PaymentController@mpesaValidate');
Route::post('/confirm', 'PaymentController@mpesaConfirm');
Route::get('/payments/confirm/mpesa', 'PaymentController@confirmPayment');
Route::get('/message', 'NotifyController@sendReminder');



