<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/mpesa-test', function () {
    return view('mpesa');
});
Route::post('/mpesa-test', 'PaymentController@webPay');
Route::get('/transactions', function () {
    return view('payment');
});
Route::get('/android', "DownloadController@getDownload");